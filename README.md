# Biocompostera

[![CC BY 4.0][cc-by-sa-shield]][cc-by-sa]

Compostera para experimentación con biomateriales, parte del Nodo de Biofabricación Digital del [FabLab U. de Chile](fablab.uchile.cl).

<img src="/img/vista1.jpg" width="700">

### Manual de ensamblaje

Puedes revisar el manual de ensamblaje de la compostera [acá](/pdf/manual_biocompostera.pdf)


### Control de humedad

Puedes revisar el instructivo para ensamblar el controlador de humedad de la compostera [acá](/pdf/manual_biocompostera.pdf)

<img src="/img/vista2.jpg" width="500">

<img src="/img/vista3.jpg" width="500">

# Licencia
[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

Este trabajo esta publicado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY%20SA%204.0-lightgrey.svg
