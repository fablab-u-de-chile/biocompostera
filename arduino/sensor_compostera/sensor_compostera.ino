#include <dht.h>
#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

dht DHT;

#define DHT11_PIN 8

void setup(){
  lcd.begin(16, 2);
}

void loop()
{
  int chk = DHT.read11(DHT11_PIN);
  lcd.setCursor(0,0); 
  lcd.print("Temp: ");
  lcd.print(DHT.temperature);
  lcd.print((char)223);
  lcd.print("C");
  lcd.setCursor(0,1);
  lcd.print("Humidity: ");
  lcd.print(DHT.humidity);
  lcd.print("%");
  delay(5000);

  if (DHT.humidity <= 24)
   {
   digitalWrite(13, HIGH); // Enciende el Led 13.
   //lcd.write("ON"); // Envía por el puerto ON.

    }
else
   {
      digitalWrite(13, LOW); // Apaga el Led 13.
   //lcd.write("OFF"); // Envía por el puerto OFF.
   }
}
}
